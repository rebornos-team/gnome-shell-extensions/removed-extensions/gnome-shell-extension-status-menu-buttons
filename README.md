# gnome-shell-extension-status-menu-buttons

A GNOME Shell Extension that adds a hibernate, suspend, & lock button to the status menu.

https://github.com/Antergos/gnome-shell-extension-status-menu-buttons

# DISCONTINUED

**Not updated three years ago. It does not work. It was used by Antergos in their customization of Gnome.**